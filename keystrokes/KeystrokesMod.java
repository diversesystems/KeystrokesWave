/*
 *     Copyright (C) 2018  Hyperium <https://hyperium.cc/>
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published
 *     by the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package keystrokes;




import keystrokes.config.KeystrokesSettings;
import keystrokes.render.KeystrokesRenderer;
import net.minecraftforge.client.ClientCommandHandler;
import net.minecraftforge.common.MinecraftForge;
import net.minecraftforge.fml.common.Mod;
import net.minecraftforge.fml.common.event.FMLInitializationEvent;
import net.minecraftforge.fml.common.event.FMLPreInitializationEvent;

@Mod(modid = "keystrokes", version = "4.1.1")
public class KeystrokesMod {

    /**
     * The mods metadata
     */

    private KeystrokesSettings config;
    private KeystrokesRenderer renderer;

    @Mod.EventHandler
    public void preinit(FMLPreInitializationEvent e)
    {
        this.config = new KeystrokesSettings(this, e.getModConfigurationDirectory());
    }

    @Mod.EventHandler
    public void init(FMLInitializationEvent e) {
        MinecraftForge.EVENT_BUS.register(this);
        this.renderer = new KeystrokesRenderer(this);
        this.config.load();
        ClientCommandHandler.instance.registerCommand(new CommandKeystrokes(this));
        MinecraftForge.EVENT_BUS.register(this.renderer);
    }

    /**
     * Getter for the Keystrokes settings
     *
     * @return the keystrokes settings
     */
    public KeystrokesSettings getSettings() {
        return this.config;
    }

    /**
     * Getter for the Keystrokes renderer
     *
     * @return the keystrokes renderer
     */
    public KeystrokesRenderer getRenderer() {
        return this.renderer;
    }
}
